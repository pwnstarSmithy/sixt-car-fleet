//
//  carTableViewController.swift
//  Sixt Car Fleet
//
//  Created by pwnstarSmithy on 26/08/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

//Declare variable to cache Newsfeed images.
var postImageCache = NSCache<NSString, UIImage>()

//extension to download images from internet
extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        //make sure the cell is emptied before checking cache for  image
        image = nil
        //check if image is in cache before loading it from the internet
        if let imageFromCache = postImageCache.object(forKey: url.absoluteString as NSString){
            
            self.image = imageFromCache
            return
        }
        
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                
                let imageToCache = image
                
                postImageCache.setObject(imageToCache, forKey: url.absoluteString as NSString)
                
                self.image = imageToCache
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}


class carTableViewController: UITableViewController {

    var carsArray = [carArray]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //trigger function that downloads car information
        callCars()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    //function to download car information from the server
    func callCars() {
        let url = NSURL(string: "http://codetalk.de/cars.json")!
        print(url)
        
        var request = URLRequest(url: url as URL)
        
        // method to pass data
        request.httpMethod = "GET"
        
        //launch the session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {return}
            
            //check for any errors
            
            if error == nil {
                
                do {
                    
                    let downloadedCars = try JSONDecoder().decode(Array<carArray>.self, from: data)
                    
                    print(downloadedCars)
                    
                    //enter downloaded data into array created earlier.
                    self.carsArray = downloadedCars
                    
                    //iterate through array
                    for rideArray in self.carsArray {
                        print(rideArray)
                        
                        DispatchQueue.main.async(execute: {
                            self.tableView.reloadData()
                        })
                        
                    }
                    
                    
                }catch{
                    
                    print(error)
                    
                }
                
                
            }else{
                
                print(error!)
                
            }
            
            }.resume()
        
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return carsArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "carCell", for: indexPath) as! carTableViewCell
        if carsArray[indexPath.row].modelName != nil{
            cell.modelName.text = carsArray[indexPath.row].modelName
        }
        if carsArray[indexPath.row].make != nil {
            cell.carMake.text = carsArray[indexPath.row].make
        }
        if carsArray[indexPath.row].name != nil {
             cell.driver.text = carsArray[indexPath.row].name
        }
        if carsArray[indexPath.row].licensePlate != nil {
            cell.licensePlate.text = carsArray[indexPath.row].licensePlate
        }
        
        if (carsArray[indexPath.row].modelIdentifier != nil) && carsArray[indexPath.row].color  != nil {
            let carModelId = carsArray[indexPath.row].modelIdentifier
            let carColor = carsArray[indexPath.row].color
            
            //try loading car images from first URL that has been specified
            let firstUrl = "https://content.drive-now.com/sites/default/files/cars/3x/\(carModelId!)/\(carColor!).png"
            let imageUrl = URL(string: firstUrl)
            
            
            
            cell.carImage.downloadedFrom(url: imageUrl!)
        }
        
      
        
        //check to see if car image view is by any means still missing, and then try alternative url.
         DispatchQueue.main.async(execute: {
        if cell.carImage.image == nil {
            
            if self.carsArray[indexPath.row].carImageUrl != nil {
                let urlString = self.carsArray[indexPath.row].carImageUrl
                let profileURL = URL(string: urlString!)
                
                cell.carImage.downloadedFrom(url: profileURL!)
                
                
                
            }
            
            
        }
        
         })
 
        return cell
    }
   
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
         return "Sixt Car Fleet!"
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let destination = segue.destination as? MapViewController{
            
            destination.passedArray = carsArray[(tableView.indexPathForSelectedRow?.row)!]
            
        }
    }
    

}
