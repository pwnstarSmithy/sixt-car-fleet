//
//  MapViewController.swift
//  Sixt Car Fleet
//
//  Created by pwnstarSmithy on 26/08/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

//i choose to use google maps instead of mapkit because the unavailablity of Apple Maps in some regions that Sixt Fleet might be operating in.
import GoogleMaps



class MapViewController: UIViewController {
    

    //variables passed from previous view controller.
    var longitudePassed : Double?
    var latitudePassed : Double?
    var carModel : String?
    var carPlate : String?
    var carImageUrl : String?
    
    var passedArray : carArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //assign data to variables pulled from array containing car information
        longitudePassed = passedArray?.longitude
        latitudePassed = passedArray?.latitude
        carModel = passedArray?.modelName
        carPlate = passedArray?.licensePlate
        carImageUrl = passedArray?.carImageUrl
        
        //run check to confirm whether longitude and latitude aren't empty
        if longitudePassed != nil && latitudePassed != nil {
            //call function to load googlemaps
            loadMaps()

            
        }else{
            //present alertview to user notifying him/her the car isn't on the map.
            let failAlert = UIAlertController(title: "Error", message: "We don't have location details for this particular car", preferredStyle: UIAlertControllerStyle.alert)
            
            failAlert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: { (action) in
                
                failAlert.dismiss(animated: true, completion: nil)
                
                
            }))
            
            DispatchQueue.main.async(execute: {
                self.present(failAlert, animated: true, completion: nil)
            })
            
        }
        
        // Do any additional setup after loading the view.
    }
    
    func loadMaps() {
        //provide the google maps api key that allows the app to connect to the service.
        GMSServices.provideAPIKey("AIzaSyCoj89BF85hLGn640PeX3F2lI7huya_tTc")
        
        //create a camera position that tells the map to display the cordinates that have been passed on at predefined zoom level.
        let camera = GMSCameraPosition.camera(withLatitude: latitudePassed!, longitude: longitudePassed!, zoom: 12)
        
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        
        //assign mapview to View
        view = mapView
        
        //add a marker to the center of the map.
        
        let marker = GMSMarker()
        
        marker.position = CLLocationCoordinate2D(latitude: latitudePassed!, longitude: longitudePassed!)
        
        marker.title = carModel
        marker.snippet = carPlate
        marker.map = mapView
        
       
    
        
        //set the marker to always show information tab
        mapView.selectedMarker = marker
    }
    
    func loadCarImages(){
        
     
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

 

}
