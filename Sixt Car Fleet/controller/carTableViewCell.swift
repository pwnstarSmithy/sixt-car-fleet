//
//  carTableViewCell.swift
//  Sixt Car Fleet
//
//  Created by pwnstarSmithy on 26/08/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit


//file to access tableview cell elements
class carTableViewCell: UITableViewCell {
    
    @IBOutlet weak var carImage: UIImageView!
    
    @IBOutlet weak var modelName: UILabel!
    
    @IBOutlet weak var carMake: UILabel!
    
    @IBOutlet weak var driver: UILabel!
    
    @IBOutlet weak var licensePlate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
