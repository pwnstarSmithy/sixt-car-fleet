Sixt Car Fleet

By Mugalu Yusuf

mugalu@bulamu.net

Instructions

Navigate to repo

Sixt Fleet is built using XCode 9.3.1, the language is Swift 4. This is an IOS application built for iPhones running IOS 10.0 and above, this is was done to accommodate most of the old generation phones up to the iPhone 5. Once you import the project into your XCode, everything else will be really straightforward.
Enjoy!

Discussion

I used the following technologies:
- Xcode 9.3.1
- Swift 4
- CocoaPods - GoogleMaps :- i chose to use GoogleMaps because of the unavailability of Apple Maps in some of the regions that Sixt operates in. (https://developers.google.com/maps/documentation/ios-sdk/intro)

Requirements

Implement an app, that downloads this file and use all information available that you think makes sense to display
- Displays these cars in a list
- Displays these cars on a map

I was able to create a table that generates a list of all the returned cars, in each of these cells, some information about the car is displayed with the car image by its side to give the user an idea what type of car they are about to rent.
If a user taps on one of the car cells, they'll be taken to a map using GoogleMaps which will show a marker on the map displaying where the car is located at that particular point. the location is dependant on the information returned from the server which includes a latitude and longitude.
I ran into a particular problem with the updated image url pattern, most of the cars weren't returning images using this pattern. so i decided to add a fall back to the old url pattern that isn't upto date that's returned with each car. This url is only triggered if the updated(prefered) url returns a 404 which means it'll leave the image placeholder in the cell empty. i run a check to confirm whether the image placeholder is empty, then i trigger another call using an alternative url to load car images.

Bonuses!

Make the board fully responsive

- I tried to always check whether a variable is empty before doing something with it.
- I tried to use as less frameworks or libraries as possible in order to best demonstrate my coding experience.
- I have made the project available on BitBucket and would happily add a member of your team to view the project on - BitBucket. Version control was used on the project from start to finish.
