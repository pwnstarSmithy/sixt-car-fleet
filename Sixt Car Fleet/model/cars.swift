//
//  cars.swift
//  Sixt Car Fleet
//
//  Created by pwnstarSmithy on 25/08/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//



//create object map for cars json returned from server.
import Foundation


struct carArray : Decodable {
    
    let id : String?
    let modelIdentifier : String?
    let modelName : String?
    let name : String?
    let make : String?
    let group : String?
    let color : String?
    let series : String?
    let fuelType : String?
    let fuelLevel : Double?
    let transmission : String?
    let licensePlate : String?
    let latitude : Double?
    let longitude : Double?
    let innerCleanliness : String?
    let carImageUrl : String?
    
}


